# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")
from math import sqrt, ceil

def semiprimes(N):
    composites = set()
    semip = set()
    primes = set(i for i in range(2,N+1))
    found = set()
    for i in range(2,ceil(sqrt(N))+2):
        if not i in composites:
            found.add(i)
            semip.update(i*a for a in found)
            uppers = [i*a for a in range(i,N//i+1)]
            composites.update(uppers)
            primes.difference_update(uppers)
    for p in sorted(primes.difference(found), reverse=True):
        for p2 in sorted(found):
            if p2*p <= N:
                semip.add(p2*p)
            else:
                break
    return sorted(semip)
    
def solution(N, P, Q):
    primes = semiprimes(N)
    counts = []
    for a,b in zip(P,Q):
        count = len(primes)
        for i in primes:
            if i < a:
                count -=1
        for i in primes[::-1]:
            if i > b:
                count -= 1
        counts.append(count)
        
    return counts
