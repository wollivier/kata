def solution(A):
    max_until = max_slice = 0
    startslice = 0
    start = 0
    minval = min(A[1:2])
    for i, a in enumerate(A[2:-1]):
        if a < minval:
            max_until += minval
            minval = a
        else:
            max_until = max(0, max_until+a)
        if max_until == 0:
            startslice = i+1
            minval = a
        max_slice = max(max_slice, max_until)
        if max_slice == max_until:
            start = startslice
    return max_slice
