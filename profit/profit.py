from collections import namedtuple

class Extremum:
    def __init__(self, index=0, value=0, profit=0):
        self.index = index
        self.value = value
        self.profit = profit
        self.sell_at = index
    def __repr__(self):
        return "buy at {} for {}, sell at {} for {}, profit {}".format(
            self.index,
            self.value,
            self.sell_at,
            self.profit+self.value,
            self.profit)


n = input()
#prices = map(int, raw_input().split())
prices = [int(i) for i in raw_input().split()]

# diffs = [prices[i+1] - prices[i] for i in range(0, len(prices)-1)]

local_min = list()
min = Extremum(0, prices[0])
max = prices[0]
for i, price in enumerate(prices[1:], 1):
    if price < min.value:
        min.profit = max - min.value
        min.sell_at = i
        local_min.append(min)
        min = Extremum(i, price)
        max = min.value
    else:
        if price> max:
            max = price
min.profit = max - min.value
min.sell_at = i
local_min.append(min)
print sorted(local_min, key=lambda x:x.profit, reverse=True)
