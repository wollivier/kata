#!/bin/python3
from __future__ import print_function
import sys

def eprint(*args):
    pass #print(*args, file=sys.stderr)

def hackerlandRadioTransmitters(x, k):
    count = 1
    if len(x) == 1:
        return count
    else:
        i = 0
        left = 0
        right = 0
        while(i+1 < len(x)):
            try:
                while(left + x[i+1]-x[i] <= k):
                    left += x[i+1]-x[i]
                    i+= 1
                while(right + x[i+1]-x[i] <= k):
                    right += x[i+1]-x[i]
                    i+=1
                if left>0 or right>0:
                    count+=1
                else:
                    i+= 1
                left = 0
                right = 0
            except IndexError:
                pass
        return count
    

if __name__ == "__main__":
    n, k = raw_input().strip().split(' ')
    n, k = [int(n), int(k)]
    x = list(map(int, raw_input().strip().split(' ')))
    x.sort()
    result = hackerlandRadioTransmitters(x, k)
    print(result)
