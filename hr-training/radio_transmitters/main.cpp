#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;

int solve(std::vector<int>::const_iterator start, vector<int>::const_iterator stop, int range)
{
    //if (std::distance(start, stop) == 0)
    int counter = 1;
    int left = 0, right = 0;
    bool full = false;
    for(auto i = start; i != stop; ++i)
    {
        if (*i > range)
        {
            return counter + /*solve(start, i, range) +*/ solve(i+1, stop, range);
        }
        else
        {
        if (not full)
        {
            if (left + *i < range)
            {
                left+=*i;
                cerr << 1 << endl ;
            }
            else if (left+ *i == range)
            {
                left += *i;
                full=true;
                right = 0;
                cerr << 2 << endl ;
            }
            else
            {
                full = true;
                right += *i;
                cerr << 3 << endl ;
            }
        }
        else
        {
            if (right + *i < range)
            {
                right += *i;
                cerr << 4 << endl ;
            }
            else if (right + *i == range)
            {
                left = 0;
                right = 0;
                full = false;
                ++ counter;
                cerr << 5 << endl ;
            }
            else 
            {
                left = *i;
                right = 0;
                full = false;
                ++counter;
                cerr << 6 << endl ;
            }
        }
        }
    }
    if (left == 0 and right == 0)
        --counter;
    return counter;
}

int main() {
      int n, range;
    cin >> n >> range;

    priority_queue<int, vector<int>, std::greater<int>> q;
    int prev = 0;
    while(n)
    {
        cin >> prev;
        q.push(prev);
        --n;
    }
    vector<int> v;
    prev = q.top();
    q.pop();
    int current;
    while(not q.empty())
    {
        current = q.top();
        v.push_back(current-prev);
        prev = current;
        q.pop();
    }
    for (auto i:v)
    {
        cerr << i << " ";
    }
    cerr << endl;

    int result = solve(v.begin(), v.end(), range);
    cout << result;
    return 0;
}

