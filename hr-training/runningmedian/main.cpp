#include <algorithm>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;

void push(int i,
    priority_queue<int, vector<int>, less<int>>& lower,
    priority_queue<int, vector<int>, greater<int>>& upper,
    const vector<double>& medians)
{
    if (medians.empty() or i >= medians.back())
        upper.push(i);
    else
        lower.push(i);
}

void rebalance(priority_queue<int, vector<int>, less<int>>& lower,
    priority_queue<int, vector<int>, greater<int>>& upper)
{
    while (upper.size() > lower.size() + 1) {
        lower.push(upper.top());
        upper.pop();
    }
    while (lower.size() > upper.size()) {
        upper.push(lower.top());
        lower.pop();
    }
    assert(upper.size() == lower.size() or upper.size() == lower.size() + 1);
}

double median(
    const priority_queue<int, vector<int>, less<int>>& lower,
    const priority_queue<int, vector<int>, greater<int>>& upper)
{
    if (lower.size() == upper.size())
        return (lower.top() + upper.top()) / 2.0;
    else
        return upper.top();
}

vector<double> runningMedian(vector<int> a)
{
    // max prioq representing lower part of input
    priority_queue<int, vector<int>, less<int>> lower;
    // min prioq representing upper part of input
    priority_queue<int, vector<int>, greater<int>> upper;

    vector<double> out;
    out.reserve(a.size());
    for (int in : a) {
        push(in, lower, upper, out);
        rebalance(lower, upper);
        out.push_back(median(lower, upper));
    }

    return out;
}

int main()
{
    size_t a_count;
    cin >> a_count;

    vector<int> a(a_count);
    for (size_t i = 0; i < a_count; ++i) {
        cin >> a[i];
    }

    vector<double> result = runningMedian(a);

    ofstream fout(getenv("OUTPUT_PATH"));
    fout << setprecision(1) << fixed;
    for (double& d : result) {
        fout << d << endl;
    }
    fout.close();

    return 0;
}
