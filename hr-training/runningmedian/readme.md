# Interview preparation

## Hackerrank
### Running median

[The problem on Hackerrank](https://www.hackerrank.com/challenges/find-the-running-median/problem)

##### General notes:
* The hackerrank section this problem resides in seems to indicate that _heaps_ should be used to solve the problem (and by extension, _priority queues_)

##### Notes on C++ implementation
* Can use `priority_queue<data_type, container, comparator>` 
* or just the `make_heap`, `pop_heap` and `push_heap` functions, but beware of syntax (need to `pop_back` after `pop_heap`, and comparator needs to be passed to all calls)

##### Notes on implementation
* Approach with sort is too slow (O(nlgn) for sort * n items = n2lgn )
* _CAUTION_: Be very careful with stop conditions when using iterators, given that “last” parameter in calls is usually excluded.

##### Solution
Use 2 priority queues, one for each half of the vector:
* max heap for `lower` half
* min heap for `upper` half

Queues receive items one at a time, as algorithm goes through the input list one item at a time (_n_ times).
Depending on the current (latest computed) median, we decide in which half the new item should go.
Then we rebalance the 2 halves so that their lengths are
* equal
* or `upper` is 1 unit longer than `lower`

The median is either:
* `(lower.top + upper.top)/2` if both halves are the same size
* or `upper.top` if upper is longer (by 1 unit)

##### Complexity analysis
* We go through the items of the input list one at a time (_n_).
* Every time:
	* Extract current median: <equation>\mathcal{O}(1)</equation>
	* Push the item to `lower` or `upper`: <equation>\mathcal{O}(\lg{n})</equation>
	* Rebalance the queues (1 push and 1 pop): <equation>\mathcal{O}(2\lg{n})</equation>
	* Extract tops: <equation>\mathcal{O}(1)</equation>

This gives an asymptotic run time of <equation>\mathcal{O}(n\lg{n})</equation>