#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

/* min number of steps is # different values -1 (for examples if all values differed by 1)
this needs to be adjusted 
Nope, if there are more duplicates, only one can be not touched 
first lets try and build a dictionary of all values, with their count

Let's think again about this:
adding the same amount to all but one element is exactly the same as
removing that amount from 1 single element
*/


int resolve_diff(int d)
{
    int q = d/5;
    switch(d%5)
    {
        case 0:
            break;
        case 1:
        case 3:
            q += 1;
            break;
        case 2:
        case 4:
            q += 2;
            break;
    }
    cerr << q << endl;
    return q;
}

int testcase()
{
    int n;
    cin >> n;
    cerr << n << "n values" << endl;
    map<int, int> m;
    int c;
    while(n)
    {
        cin >> c;
        m[c]+=1;
        --n;
    }
    
    int result = 0;
    int smallest = m.begin()->first;
    cerr << "smallest is " << smallest << " " << m.begin()->second << endl;
    for(auto p: m)
    {
        if(p.first != smallest)
        {
            cerr << p.first << "(" << p.second <<"): ";
            result+= p.second*resolve_diff(p.first-smallest);
        }
    }
    cout << result << endl;
    return result;
}

int main() {
    int t;
    cin >> t;
    while(t)
    {
        testcase();
        --t;
    }
    return 0;
}

