import unittest

def mprocess(array, limit):
    _, count = process_subarrays(array, limit)
    return count

def process_subarrays(array, limit):
    print "{} {}".format(array, limit)
    if not array:
        return dict(), 0
    last = array.pop()
    valid_subarrays, count = process_subarrays(array, limit)
    print valid_subarrays
    to_add = dict()
    for p, u in valid_subarrays.iteritems():
        if(last * p) < limit:
            count += u
            print "added {} to [{}]".format(u, last*p)
            try:
                to_add[last*p] += u
            except KeyError:
                to_add[last*p] = u
    # merge dicts
    print "Merge dicts"
    for k, v in to_add.iteritems():
            try:
                valid_subarrays[k] += v
            except KeyError:
                valid_subarrays[k] = v
    if last < limit:
        print "added {}".format(last)
        count += 1
        try:
            valid_subarrays[last] += 1
        except KeyError:
            valid_subarrays[last] = 1
    print "len valid_subs {}".format(valid_subarrays)
    return valid_subarrays, count


class TestSubarray(unittest.TestCase):
    def test_case1(self):
        self.assertEqual(mprocess([2, 4, 6, 12, 15, 23, 42], 222), 7)

    def test_case2(self):
        self.assertEqual(mprocess([1, 2, 3], 4), 5)

    def test_case(self):
        self.assertEqual(mprocess([1, 2, 3, 4], 5), 7)

if __name__ == "__main__":
    unittest.main()
