#include <bits/stdc++.h>

using namespace std;

map<char, size_t> histogram(const string& s)
{
    map<char, size_t> m;
    for(char c : s)
    {
        ++m[c];
    }
    return m;
}

int maxLen(string s){
    // identify suitable pairs of characters (ocurrences are same or +- 1)
    map<char, size_t> hist = histogram(s);
    for_each(hist.begin(), hist.end(), [](const pair<char, size_t>& p){cerr << "(" << p.first <<","<< p.second <<")";});
    cerr << endl;
    set<pair<char, char>> suitable;
    auto iter = hist.begin();
    while(iter != hist.end())
    {
        size_t count = iter->second;
        auto iter2 = iter;
        ++iter2;
        while(iter2 != hist.end())
        {
            if ((max(count, iter2->second) - min(count, iter2->second)) <= 1)
            {
                suitable.emplace(iter->first, iter2->first);
            }
            ++iter2;
        }
        ++iter;
    }
    for_each(suitable.begin(), suitable.end(), [](const pair<char, char>& p){cerr << "(" << p.first <<","<< p.second <<")";});
    cerr << endl;
    
    // then test each pair of characters
    int max_length = 0;
    for (auto suit: suitable)
    {
        // 1.look for position of first character
        size_t index_first = s.find_first_of(string()+suit.first);
        size_t index_second = s.find_first_of(string()+suit.second);
        char expected, next;
        expected = s[min(index_first, index_second)];
        next = s[max(index_first, index_second)];
        int length= 0;
        for (char c:s)
        {
            if(c == expected)
            {
                swap(next, expected);
                ++length;
            }
            else if (c == next)
            {
                length = 0;
                break; // string is not valid
            }
        }
        if (length > max_length)
        {
            max_length = length;
        }
    }
    
    return max_length;
}

int main() {
    int n;
    cin >> n;
    string s;
    cin >> s;
    int result = maxLen(s);
    cout << result << endl;
    return 0;
}

