#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
#include <cassert>
using namespace std;

using pq = std::priority_queue<int64_t, vector<int64_t>, std::greater<int64_t>>;

class NotFound {};

void process_command(int op, pq& heap, pq& rheap)
{
    int64_t c;
    switch(op)
    {
        case 1:
            cin >> c;
            heap.push(c);
            break;
        case 2:
            cin >> c;
            rheap.push(c);
            break;
        case 3:
            assert(heap.size() >= 1);
            while(rheap.size() and rheap.top() == heap.top())
            {
                    rheap.pop();
                    heap.pop();
                    if(heap.empty())
                            throw NotFound();
            }
            cout << heap.top() << endl;
            break;
    }
}

int main() {
    int n;
    cin >> n;
    int op;
    pq heap;
    pq rheap;
    while(n)
    {
        cin >> op;
        process_command(op, heap, rheap);
        --n;
    }
    return 0;
}

