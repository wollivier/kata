cmake_minimum_required(VERSION 3.5)
project(hr-training)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)

add_executable(alternating-letters alternating-letters.cpp)
target_compile_features(alternating-letters PUBLIC cxx_range_for)

add_executable(weighted-unif-strings weighted-unif-strings.cpp)
target_compile_features(weighted-unif-strings PUBLIC cxx_range_for)
