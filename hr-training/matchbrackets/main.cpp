#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
#include <stack>
using namespace std;


int main() {
    int n;
    cin >> n;
    string ignore;
    getline(cin, ignore); // get rid of extra \n
    map<char, char> m;
    m['}'] = '{';
    m[')'] = '(';
    m[']'] = '[';
    while(n)
    {
        stack<char> s;
        string st;
        getline(cin, st);
        for(char c: st)
        {
            auto it = m.find(c);
            if(it != m.end())
            {
                if(s.empty() or s.top() != it->second)
                {
                    cout << "NO" << endl;
                    goto end;
                }
                else if (s.top() == it->second)
                    s.pop();
            }
            else
            {
                s.push(c);
            }
        }
        if(s.empty())
            cout << "YES" << endl;
        end:
        --n;
    }
    return 0;
}

