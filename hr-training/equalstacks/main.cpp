#include <queue>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

vector<string> split_string(string);

uint64_t equaldeques(deque<int> h1, deque<int> h2, deque<int> h3, uint64_t a, uint64_t b, uint64_t c) {
    vector<deque<int>*> h = {&h1, &h2, &h3};
    vector<uint64_t> co = {a, b, c};
    size_t i = 0;
    cerr << a << " " << b << " " << c << endl;
    while(co[0] != co[1] or co[1] != co[2])
    {
        i = distance(co.begin(), max_element(co.begin(), co.end()));
        if(h[i]->empty())
            return 0;
        cerr << "Popping " << h[i]->back() << " from stack " << i << " count was " << co[i];
        co[i] -= h[i]->back();
        cerr << " now " << co[i] << endl;
        h[i]->pop_back();
    }
    return co[0];
}

int main()
{
    int n1, n2, n3;
    cin >> n1 >> n2 >> n3;
    deque<int> u, v, w;
    int i;
    uint64_t a=0, b=0, c=0;
    using P = tuple<int, deque<int>&, uint64_t&>;
    for(auto x : initializer_list<P>{{n1,u, a},{n2,v,b},{n3,w,c}})
    {
        while(get<0>(x))
        {
            cin >> i;
            get<1>(x).push_front(i);
            get<2>(x) += i;
            --get<0>(x);
        }
    }
    ofstream fout(getenv("OUTPUT_PATH"));
    uint64_t result = equaldeques(u, v, w, a, b, c);
    fout << result << "\n";
    fout.close();

    return 0;
}

