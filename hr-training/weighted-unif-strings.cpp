#include <set>
#include <string>
#include <iostream>

using namespace std;

void isInUniverse(const set<int>& universe, int x)
{
    cout << string((universe.find(x) != universe.end()) ? "Yes" : "No") << endl;
}

set<int> generateUniverse(const string& s)
{
    // greedy consumption of the string
    char previous = '\0';
    int count     = 0;
    set<int> universe;
    for (char c : s)
    {
        if (c != previous)
        {
            previous = c;
            count    = 0;
        }
        count += c - 'a' + 1;
        universe.insert(count);
    }
    return universe;
}

int main()
{
    string s;
    cin >> s;
    int n;
    cin >> n;
    set<int> universe = generateUniverse(s);
    for (int a0 = 0; a0 < n; a0++)
    {
        int x;
        cin >> x;
        isInUniverse(universe, x);
    }
    return 0;
}
