#include <queue>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * Complete the truckTour function below.
 */
int truckTour(deque<int> q)
{
    size_t size = q.size();
    size_t pump = 0;
    int remain = 0;
    bool broken = false;

    while(pump < size)
    {
        for(int contrib: q)
        {
                remain += contrib;
                if (remain < 0)
                {
                        q.push_back(q.front());
                        q.pop_front();
                        ++pump;
                        remain = 0;
                        broken = true;
                        break;
                }
        }
        if (not broken)
        {
                return pump;
        }
        broken = false;
    }
    return -1;
}

int main()
{
    ofstream f(getenv("OUTPUT_PATH"));
    ostream* s = &cout;
    if (getenv("OUTPUT_PATH"))
    {
        s = &f;
    }
    ostream& fout(*s);

    int n;
    cin >> n;

    deque<int> q;
    int p, d;
    uint64_t total_fuel = 0;
    uint64_t total_distance = 0;
    while (n) {
        cin >> p >> d;
        q.push_back(p - d);
        total_fuel += p;
        total_distance += d;
        --n;
    }
    cerr << "contrib read" << endl;

    int result = 0;
    if (total_distance > total_fuel)
        result = -1;
    else
        result = truckTour(q);

    fout << result << "\n";

    f.close();

    return 0;
}
