Improvement to think about:
* It looks like the market price problem where I have to find the max profit (`max_profit` problem)
* When iterating over the dataset to find if the starting pump is suitable:
    * the first pump has a positive contribution (otherwise it would have been discarded)
    * so if it is removed, the overall contribution goes down
    * and this means that none of the pumps in between that frst one and the one that tripped the count can cover for it
    * the big reveal: none of the pump in between those 2 can be the starting pump.
    * this should make for an algorithm in O(2n), instead of O(n**2) as it is today, even if it passes the tests. (actual implementation may actually be further bounded)
