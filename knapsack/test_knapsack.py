import unittest
import knapsack

class Test(unittest.TestCase):

    def test_zero(self):
        self.assertEqual(1, knapsack.getWays(0, [1, 2, 3]))
    def test_one(self):
        self.assertEqual(1, knapsack.getWays(1, [1, 2, 3]))

    def test_two(self):
        self.assertEqual(2, knapsack.getWays(2, [1, 2, 3]))
    def test_four(self):
        self.assertEqual(4, knapsack.getWays(4, [1, 2, 3]))
    def test_four(self):
        self.assertEqual(5, knapsack.getWays(10, [2, 5, 3, 6]))

unittest.main()