memo = {}

def getWays(val, tokens):
    global memo
    tokens.sort()
    if val == 0:
        return 1
    if not tokens:
        return 0
    assert val > 0
    if (val, tokens[0]) in memo:
        return memo[(val, tokens[0])]
    counter = 0
    for index, t in enumerate(tokens):
        if val - t == 0:
            counter += 1
            break
        elif val - t > 0:
            counter += getWays(val - t, tokens[index:])
        else:
            break
    memo[(val, tokens[0])] = counter
    return counter