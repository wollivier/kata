"""Kata from http://codingdojo.org/kata/Anagram/"""
from __future__ import print_function
import requests
import sys
import itertools



WORD_LIST = "https://gist.githubusercontent.com/calvinmetcalf/084ab003b295ee70c8fc/raw/314abfdc74b50f45f3dbbfa169892eff08f940f2/wordlist.txt"
WORD_LIST = "https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt"



class Vector(object):
    def __init__(self, values=None):
        self.content = values if values else [0] * 26
        self.tuple = tuple(self.content)
        self.sum = sum(self.content)

    def __hash__(self):
        return hash(self.tuple)

    def __sub__(self, other):
        # following line works because of __iter__
        values = [x-y for x,y in zip(self, other)]
        return Vector(values)

    def __iter__(self):
        return iter(self.content)

    def __eq__(self, other):
        return self.content == other.content

    def __repr__(self):
        return repr(self.content)

    def is_valid(self):
        gen = map(lambda x: x >= 0, self.content)
        return all(gen)

    @classmethod
    def from_word(cls, word):
        values = [0] * 26
        for c in word:
            values[ord(c) - ord('a')] += 1
        return cls(values)


def get_word_set():
    word_set = set()
    with requests.Session() as session:
        data = session.get(WORD_LIST).text
        # skip first line... not always needed
        data = data[data.index('\n'):]
        word_set.update(data.split())
    return word_set


def get_anagram_pairs(reference_word, word_set):
    letters_to_words = dict()

    for word in word_set:
        vector = Vector.from_word(word)
        letters_to_words.setdefault(vector, []).append(word)

    reference_vector = Vector.from_word(reference_word)

    couples = []
    remaining_keys = set((key for key in letters_to_words.keys() if sum(key)<= sum(reference_vector)))
    while remaining_keys:
        key = remaining_keys.pop()
        complement = reference_vector - key
        if complement in remaining_keys:
            couples.append((key, complement))
        elif complement == key:
            couples.append((key, key))

    matching_pairs = []
    for v1, v2 in couples:
        matching_pairs.extend(itertools.product(letters_to_words[v1], letters_to_words[v2]))
    return sorted(matching_pairs)


def get_reference_word():
    reference_word = "documenting"
    try:
        reference_word = sys.argv[1]
    except IndexError:
        pass
    return reference_word


def main():
    ref_word = get_reference_word()
    print("Reference word is '{}'".format(ref_word))
    word_set = get_word_set()
    print("{} words in complete word set".format(len(word_set)))
    pairs = get_anagram_pairs(ref_word, word_set)
    print(pairs)

if __name__ == '__main__':
    main()