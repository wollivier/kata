"""
Compute the number of sub-arrays in an array of size n
"""

def size_of_subarray(array_size):
    "Recursive implementation od the subarray problem"
    if array_size == 0:
        return 0
    return 2 * size_of_subarray(array_size-1) + 1
