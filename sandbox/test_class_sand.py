"""
Testing classes
"""

import unittest
from class_sand import Greeter, NewObjectGreeter, OldObj

class OldObjGreeter(OldObj):
    at = 42
    @classmethod
    def chello(cls):
        return str(cls.at)

class MyG(Greeter):
    "Implemented abstract method"
    def ahello(self):
        print("Hello, Not so abstract anymore method")

class NewStaticGreeter(MyG):
    "Overriden static method"
    @staticmethod
    def shello():
        print("Hello, New static method")

class TestClasses(unittest.TestCase):
    "Big test case with everything"
    def test_abstract_construction(self):
        "Expect a TypeError when constructing an abstract class"
        with self.assertRaises(TypeError):
            Greeter()

    def test_static_class(self):
        "Test IS on all the different flavours of methods"
        my = MyG()
        my.ahello()
        my.chello()
        my.shello()

        MyG.chello()
        MyG.shello()

        Greeter.chello()
        Greeter.shello()

        new = NewStaticGreeter()
        new.shello()
        self.assertFalse(new.shello is Greeter.shello)

        self.assertFalse(Greeter.chello is MyG.chello)
        self.assertTrue(Greeter.shello is MyG.shello)
        # not sure why the next one if False
        self.assertFalse(my.chello is MyG.chello)
        print(my.chello)
        print(MyG.chello)
        print(type(my.chello))
        print(type(MyG.chello))
        self.assertTrue(my.shello is MyG.shello)
        self.assertFalse(my.chello is Greeter.chello)
        self.assertTrue(my.shello is Greeter.shello)

    def test_new_object(self):
        my = NewObjectGreeter()
        my.hello()
        my.chello()
        NewObjectGreeter.chello()
        # my.ahello()
        # NewObjectGreeter.ahello()
        NewObjectGreeter.shello()

        b = OldObjGreeter()
        b.chello()
        self.assertEquals(OldObjGreeter.chello(), "42")

if __name__ == "__main__":
    unittest.main()
