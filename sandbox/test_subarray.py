"""
Unit test for the subarray module
"""
import unittest
from subarray import size_of_subarray

class TestSizeOfSubarray(unittest.TestCase):
    """
    Test case
    """
    def test_base_case(self):
        "Base case: Empty array"
        self.assertEqual(size_of_subarray(0), 0)

    def test_simple_case(self):
        "Simple case. 1-size array"
        self.assertEqual(size_of_subarray(1), 1)

    def test_other_case(self):
        "Size 4 array"
        self.assertEqual(size_of_subarray(4), 2**4-1)

    def test_range(self):
        "Size up to 50 elements"
        for i in range(1, 50):
            self.assertEqual(size_of_subarray(i), 2**i-1)


if __name__ == "__main__":
    unittest.main()
