import unittest
import kata_anagram
from kata_anagram import Vector

class Test(unittest.TestCase):

    def test_vector(self):
        self.assertEquals([0]*26, Vector().content)
        self.assertEqual(Vector([-1]* 26).is_valid(), False)
        self.assertEqual(Vector([0]* 26).is_valid(), True)

        self.assertEquals(Vector([1] * 26), Vector.from_word('abcdefghijklmnopqrstuvwxyz'))
        self.assertEquals(Vector([2] + [0] * 25), Vector.from_word("aa"))

    def test_sub_same_vector(self):
        v1 = Vector.from_word("test")
        v2 = Vector.from_word("test")
        self.assertEqual(Vector(), v1-v2)

    def test_working_cases(self):
        word_set = kata_anagram.get_word_set()
        self.assertEquals([("uncle", "twin")], kata_anagram.get_anagram_pairs("twinuncle", word_set))
        self.assertEquals([], kata_anagram.get_anagram_pairs("twinmotorbike", word_set))

unittest.main()