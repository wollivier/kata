"""
Showcase class methods in Python
"""
import abc


class Greeter:
    "Class to showcase different method types"

    # This meta class adds abstract method concepts
    # The other are offered by object, which is the parent class (or metaclass?) of all classes, even in 2.7
    __metaclass__  = abc.ABCMeta

    @staticmethod
    def shello():
        "Static class method"
        print("Hello, Static method")

    @classmethod
    def chello(cls):
        """
        Class method, takes class object in parameter
        Useful for:
        * Factory methods, usable by derived classes
        * Static methods calling static methods
        """
        print("Hello, Class method" + repr(cls))

    @abc.abstractmethod
    def ahello(self):
        "Abstract method"
        print("Hello, Abstract method")
        raise NotImplementedError


    def hello(self):
        "hello Method"
        print("Hello, Method" + repr(self))


class NewObjectGreeter(object):

    @staticmethod
    def shello():
        "Static class method"
        print("Hello, Static method")

    @classmethod
    def chello(cls):
        """
        Class method, takes class object in parameter
        Useful for:
        * Factory methods, usable by derived classes
        * Static methods calling static methods
        """
        print("Hello, Class method" + repr(cls))

    @abc.abstractmethod
    def ahello(self):
        "Abstract method"
        print("Hello, Abstract method")
        raise NotImplementedError

    def hello(self):
        "hello Method"
        print("Hello, Method" + repr(self))

class OldObj:
    @staticmethod
    def shello():
        return "shello OldObj"

    @classmethod
    def chello(cls):
        return "chello "+ cls.__name__