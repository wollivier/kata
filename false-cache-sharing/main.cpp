
#include <iostream>
#include <thread>
#include <limits>
#include <type_traits>
#include <mutex>
#include <functional>

using namespace std;

static mutex coutmutex;

static const uint32_t kOffset = 3221225472;

template <typename T, uint32_t OFFSET>
void modifyint(T* valuetomod)
{
	size_t counter = 0;
	static const size_t whenToStop = numeric_limits<T>::max()-OFFSET;
	*valuetomod = 0;
	while(counter < whenToStop)
	{
		*valuetomod = *(valuetomod) + 1;
		++counter;
	}
	lock_guard<mutex> l(coutmutex);
	cout << "Thread " << this_thread::get_id() << " says " << *valuetomod << endl;
}

template <typename T, uint32_t OFFSET>
int run(T* a, T* b)
{
	auto startTime = chrono::steady_clock::now();
	thread t1(bind(modifyint<T, OFFSET>, a));
	thread t2(bind(modifyint<T, OFFSET>, b));
	t1.join();
	t2.join();
	auto stopTime = chrono::steady_clock::now();
	cout << "Expected value is " << numeric_limits<T>::max()-OFFSET << endl;
	return chrono::duration_cast<chrono::milliseconds>(stopTime - startTime).count();
}

void withFalseCacheSharing()
{
	uint32_t a = 0;
	uint32_t b = 0;
	int duration = run<uint32_t, kOffset>(&a, &b);
	cout << "Elapsed wall time with false cache sharing in ms: ";
	cout <<  duration << endl;
}

void withoutFalseCacheSharing()
{
	alignas(128) uint32_t a=0;
	alignas(128) uint32_t b=0;
	int duration = run<uint32_t, kOffset>(&a, &b);
	cout << "Elapsed wall time without false cache sharing in ms: ";
	cout << duration << endl;
}

template<typename T, uint32_t OFFSET>
void singleThread()
{
	T a;
	auto startTime = chrono::steady_clock::now();
	thread t(bind(modifyint<T, OFFSET>, &a));
	t.join();
	auto stopTime = chrono::steady_clock::now();
	cout << "Expected value is " << numeric_limits<T>::max()-OFFSET << endl;
	cout << "Elapsed wall time for a single thread in ms:";
	cout << chrono::duration_cast<chrono::milliseconds>(stopTime - startTime).count() << endl;
}

int main()
{
	cout << "alignof(int) is " << alignment_of<int>::value << endl;
	cout << "alignof(char) is " << alignment_of<char>::value << endl;
	cout << "alignof(uint64_t) is " << alignment_of<uint64_t>::value << endl;
	singleThread<uint32_t, kOffset>();
	withFalseCacheSharing();
	withoutFalseCacheSharing();
	return 0;
}
