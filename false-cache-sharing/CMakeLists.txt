cmake_minimum_required(VERSION 3.5)
project(false-cache-sharing)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)

add_executable(fcs main.cpp)
target_compile_features(fcs PUBLIC cxx_range_for)
set_target_properties(fcs PROPERTIES LINK_FLAGS "-pthread" )
