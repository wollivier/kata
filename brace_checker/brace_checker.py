PARENS = {
        ']':'[',
        '}':'{',
        ')':'(',
        }

SYM_PARENS = set('|')

def check(value):
    stack = []
    for char in value:
        if char in SYM_PARENS:
            if stack and stack[-1] == char:
                stack.pop()
            else:
                stack.append(char)
        elif char in PARENS.values():
            stack.append(char)
        elif char in PARENS:
            if stack and stack[-1] == PARENS[char]:
                stack.pop()
            else:
                return False
    return not stack
