from brace_checker import check
import unittest

class Test(unittest.TestCase):
    def test_paren(self):
        self.assertTrue(check('()'))
        self.assertFalse(check('((('))

    def test_mixed(self):
        self.assertTrue('({[[]()]}{}{}{[][()]})')

    def test_none(self):
        self.assertTrue(check(''))

    def test_closing(self):
        self.assertFalse(check('){}'))

    def test_other_chars(self):
        self.assertTrue(check("this is a (test)"))
        self.assertFalse(check("that will fail)"))

    def test_ruby_vertical_bars(self):
        self.assertTrue(check('|some||string|'))
        self.assertFalse(check('grep toto titi | sort -u'))


if __name__ == '__main__':
    unittest.main()
