package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(check("civic"))
	fmt.Println(check("ivicc"))
	fmt.Println(check("civil"))
	fmt.Println(check("livci"))
	var b strings.Builder
	b.Write([]byte(123))
	b.WriteByte(4444)
	fmt.Println(b.String())
}

func check(value string) (result bool) {
	result = true
	var hist = make(map[rune]int)
	for _, char := range value {
		hist[char] = (hist[char] + 1) % 2
	}
	counter := 0
	for _, item := range hist {
		counter += item
		if counter > 1 {
			result = false
			break
		}
	}
	return
}
