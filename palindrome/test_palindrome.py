from palindrome import check
import unittest

class Test(unittest.TestCase):
    def test_provided(self):
        self.assertTrue(check('civic'))
        self.assertTrue(check('ivicc'))
        self.assertFalse(check('civil'))
        self.assertFalse(check('livci'))

if __name__ == '__main__':
    unittest.main()
