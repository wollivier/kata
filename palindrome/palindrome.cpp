#include <iostream>
#include <string>
#include <map>

using std::string;

bool check(const string& value)
{
    using std::map;
    bool ret = true;
    map<char,int> hist;
    for(char c : value)
    {
        hist[c] = (hist[c] +1) % 2;
    }
    bool found = false;
    for(auto item: hist)
    {
        if(item.second >= 1)
        {
            if (found)
            {
                ret = false;
                break;
            }
            else
            {
                found = true;
            }
        }
    }
    return ret;
}

int main()
{
    using std::cout;
    using std::endl;
	cout << check("civic") << endl;
	cout << check("ivicc") << endl;
	cout << check("civil") << endl;
	cout << check("livci") << endl;
    return 0;
}