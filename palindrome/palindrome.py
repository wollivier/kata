def check(value):
    def isodd(val):
        return val%2 == 1
    histogram = dict()
    for char in value:
        histogram.setdefault(char,0)
        histogram[char] += 1
    return len(list(filter(isodd, histogram.itervalues()))) <= 1 
