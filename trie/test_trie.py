import sys
import os
sys.path.append(os.path.dirname(__file__))
import unittest
import trie

class TestTrie(unittest.TestCase):

    def test_add_repr(self):
        t = trie.Trie()
        t.add("test")
        self.assertEqual(repr(t),'{"t": {"e": {"s": {"t": {"\u0000": true}}}}}')

    def test_find(self):
        t = trie.Trie()
        t.add("www.google.com")
        self.assertTrue(t.find('www.google.com'))
        self.assertFalse(t.find('www.google.fr'))
        self.assertFalse(t.find('www.google.como'))
        self.assertFalse(t.find('www.google.co'))


if __name__ == '__main__':
    unittest.main()
