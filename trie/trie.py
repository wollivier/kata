"""
Implementation of a trie for lookup and append
"""
import json
import sys
import os

class Trie(object):
    "Trie class for strings"

    def __init__(self, terminator='\x00', container=dict):
        "Constructor"
        assert isinstance(terminator, str)
        self.terminator = terminator
        assert hasattr(container(), "__init__")
        assert hasattr(container(), "__contains__")
        assert hasattr(container(), "get")
        self.container = container
        self.root = self.container()

    def find(self, string):
        "Lookup @param string inside the trie"
        node = self.root
        for c in string + self.terminator:
            if c in node:
                node = node[c]
            else:
                return False
        return True

    def add(self, string):
        "Add a string to the trie"
        node = self.root
        assert self.terminator not in string, "Added string contains terminator \
        which would cause false positives"
        for char in string:
            nextn = node.get(char, self.container())
            node[char] = nextn
            node = nextn
        nextn = node.get(self.terminator, True)
        node[self.terminator] = nextn
        return

    def __repr__(self):
        return json.dumps(self.root)

    def dprint(self):
        "Prints debug info on standard error output"
        sys.stderr.write(repr(self))
        sys.stderr.write(os.linesep)
