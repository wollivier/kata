import unittest
from fibomatrix import fibo, fibomatrix

class FiboTest(unittest.TestCase):
  def test_base_case(self):
    self.assertEqual(fibo(2), 1)
  def test_base_matrix_case(self):
    self.assertEqual(fibo(5), 5)
  def test_case_pow_2(self):
    self.assertEqual(fibo(17), 1597)
  def test_case_general(self):
    self.assertEqual(fibo(98), 135301852344706746049)
  def test_case_max(self):
    self.assertEqual(fibo(300), 222232244629420445529739893461909967206666939096499764990979600)

if __name__ == "__main__":
    unittest.main()
