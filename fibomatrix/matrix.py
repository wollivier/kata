
class Matrix:

    def __init__(self, lines):
        self.cols = len(lines[0])
        lengths = [len(line) for line in lines]
        assert lengths.count(self.cols) == len(lines)
        self.lines = lines

    def col(self, index):
        assert index < self.cols
        return [line[index] for line in self.lines]

    def line(self, index):
        assert index < len(self.lines)
        return self.lines[index]

    def multiply(self, other):
        assert isinstance(other, Matrix)
        assert self.cols == len(other.lines)
        lines = list()
        for line in self.lines:
            res_line = list()
            for colid in range(other.cols):
                res = [line[i]*other.col(colid)[i] for i in range(len(line))]
                res_line.append(sum(res))
            lines.append(res_line)
        return Matrix(lines)

    def __repr__(self):
        return repr(self.lines)
