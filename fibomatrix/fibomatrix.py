from matrix import Matrix

memo = dict()
memo[0] = 0
memo[1] = 1

CALLS = 0

class Q(Matrix):
    def __init__(self, n):
        if n+1 not in memo:
            memo[n+1] = memo[n] + memo[n-1]
        super().__init__([[memo[n+1],memo[n]], [memo[n],memo[n-1]]])

def fibo(n):
    global memo
    memo.clear()
    memo[0] = 0
    memo[1] = 1
    if n in (0, 1):
        return memo[n]
    # list all Qn to be computed (divide by 2 each time)
    # We know Q1 = [[F2 F1][F1 F0]]
    
    # we need Q n-1 to find Fn
    global CALLS
    CALLS = 0
    ret = fibomatrix(n-1).lines[0][0]
    print("num of calls {}".format(CALLS))
    return ret


def fibomatrix(n):
    print("called Q{}".format(n))
    global CALLS
    CALLS = CALLS + 1
    if n == 1:
        return Q(1)
    else:
        if n//2 in memo and n//2 -1 in memo:
            a = Q(n//2)
        else:
            a = fibomatrix(n//2) 

        if n-n//2 in memo and n-n//2 -1 in memo:
            b = Q(n-n//2)
        else:
            b = fibomatrix(n//2)
        newQ = a.multiply(b)
        memo[n+1] = newQ.lines[0][0]
        memo[n] = newQ.lines[0][1]
        memo[n-1] = newQ.lines[1][1]
        return newQ